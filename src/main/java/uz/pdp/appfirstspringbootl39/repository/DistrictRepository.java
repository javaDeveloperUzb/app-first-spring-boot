package uz.pdp.appfirstspringbootl39.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appfirstspringbootl39.entity.District;

public interface DistrictRepository extends JpaRepository<District, Integer> {
    boolean existsByNameUzEqualsIgnoreCaseOrNameRuEqualsIgnoreCaseOrNameEnEqualsIgnoreCase(String nameUz, String nameRu, String nameEn);
}
