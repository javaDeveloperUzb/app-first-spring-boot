package uz.pdp.appfirstspringbootl39.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appfirstspringbootl39.entity.Region;

public interface RegionRepository extends JpaRepository<Region,Integer> {
    boolean existsByNameUzEqualsIgnoreCaseOrNameRuEqualsIgnoreCaseOrNameEnEqualsIgnoreCase(String nameUz, String nameRu, String nameEn);
//AndCountry oxirida
}

