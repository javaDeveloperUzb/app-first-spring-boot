package uz.pdp.appfirstspringbootl39.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appfirstspringbootl39.entity.Country;

public interface CountryRepository extends JpaRepository<Country, Integer> {
    boolean existsByNameUzEqualsIgnoreCaseOrNameRuEqualsIgnoreCaseOrNameEnEqualsIgnoreCase(String nameUz, String nameRu, String nameEn);

}
