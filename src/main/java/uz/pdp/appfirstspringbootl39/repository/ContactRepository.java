package uz.pdp.appfirstspringbootl39.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appfirstspringbootl39.entity.Contact;

public interface ContactRepository extends JpaRepository<Contact, Integer> {
    boolean existsByNumber(String number);//shu raqam bor yoki yoqligi
    boolean existsByNumberAndIdNot(String number, Integer id); //shu raqam bormi 7717 faqat mani id imdan tashqarilarida bunda hammani tekshiradi faqat ozimdan tashqari
}
