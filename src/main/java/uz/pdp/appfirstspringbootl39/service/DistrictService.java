package uz.pdp.appfirstspringbootl39.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.appfirstspringbootl39.entity.District;
import uz.pdp.appfirstspringbootl39.payload.ReqDistrict;
import uz.pdp.appfirstspringbootl39.repository.DistrictRepository;

import java.util.List;

@Service
public class DistrictService {
    @Autowired
    DistrictRepository districtRepository;

    public boolean addDistrict(ReqDistrict reqDistrict) {
        boolean exists = districtRepository.existsByNameUzEqualsIgnoreCaseOrNameRuEqualsIgnoreCaseOrNameEnEqualsIgnoreCase(reqDistrict.getNameUz(), reqDistrict.getNameRu(), reqDistrict.getNameEn());
//        boolean exists = districtRepository.existsById(reqDistrict.getRegionId());
        if (!exists) {
            District district = new District();
            district.setNameUz(reqDistrict.getNameUz());
            district.setNameRu(reqDistrict.getNameRu());
            district.setNameEn(reqDistrict.getNameEn());
            districtRepository.save(district);
            return true;
        }
        return false;
    }

    public List<District> getDistricts() {
        List<District> all = districtRepository.findAll();
        return all;
    }
}
