package uz.pdp.appfirstspringbootl39.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.appfirstspringbootl39.entity.Region;
import uz.pdp.appfirstspringbootl39.payload.ReqRegion;
import uz.pdp.appfirstspringbootl39.repository.RegionRepository;

import java.util.List;
import java.util.Optional;

@Service
public class RegionService {
    @Autowired
    RegionRepository regionRepository;

    public boolean addRegion(ReqRegion reqRegion) {
        boolean exists = regionRepository.existsByNameUzEqualsIgnoreCaseOrNameRuEqualsIgnoreCaseOrNameEnEqualsIgnoreCase(reqRegion.getNameUz(), reqRegion.getNameRu(), reqRegion.getNameEn());
        if (!exists) {
            Region region = new Region();
            region.setNameUz(reqRegion.getNameUz());
            region.setNameRu(reqRegion.getNameRu());
            region.setNameEn(reqRegion.getNameEn());
            regionRepository.save(region);
            return true;
        }
        return false;
    }

    public List<Region> getRegions() {
        return regionRepository.findAll();
    }

    public boolean editRegion(Integer id, ReqRegion reqRegion) {
        Optional<Region> byId = regionRepository.findById(id);
        if (byId.isPresent()) {
            boolean exists = regionRepository.existsByNameUzEqualsIgnoreCaseOrNameRuEqualsIgnoreCaseOrNameEnEqualsIgnoreCase(reqRegion.getNameUz(), reqRegion.getNameRu(), reqRegion.getNameEn());
            if (!exists) {
                Region region = new Region();
                region.setNameUz(reqRegion.getNameUz());
                region.setNameRu(reqRegion.getNameRu());
                region.setNameEn(reqRegion.getNameEn());
                regionRepository.save(region);
                return true;
            }
        }
        return false;
    }
}
