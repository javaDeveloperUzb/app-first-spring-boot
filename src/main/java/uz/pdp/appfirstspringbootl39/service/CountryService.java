package uz.pdp.appfirstspringbootl39.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.appfirstspringbootl39.entity.Country;
import uz.pdp.appfirstspringbootl39.payload.ReqCountry;
import uz.pdp.appfirstspringbootl39.repository.CountryRepository;

import java.util.Optional;

@Service
public class CountryService {
    @Autowired
    CountryRepository countryRepository;

    public boolean addCountry(ReqCountry reqCountry) {
        boolean exists = countryRepository.existsByNameUzEqualsIgnoreCaseOrNameRuEqualsIgnoreCaseOrNameEnEqualsIgnoreCase(reqCountry.getNameUz(), reqCountry.getNameRu(), reqCountry.getNameEn());
        if (!exists) {
            Country country = new Country();
            country.setNameUz(reqCountry.getNameUz());
            country.setNameRu(reqCountry.getNameRu());
            country.setNameEn(reqCountry.getNameEn());
            countryRepository.save(country);
            return true;
        }
        return false;
    }

    public boolean editContact(Integer id, ReqCountry reqCountry) {
        Optional<Country> byId = countryRepository.findById(id);
        if (byId.isPresent()) {
            Country country = new Country();
            country.setNameUz(reqCountry.getNameUz());
            country.setNameRu(reqCountry.getNameRu());
            country.setNameEn(reqCountry.getNameEn());
            countryRepository.save(country);
            return true;
        }
        return false;
    }
}
