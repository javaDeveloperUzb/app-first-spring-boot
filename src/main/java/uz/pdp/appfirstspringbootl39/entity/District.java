package uz.pdp.appfirstspringbootl39.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@PackagePrivate
@Entity
public class District {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Column(unique = true, nullable = false)
    String nameUz;

    @Column(unique = true, nullable = false)
    String nameRu;

    @Column(unique = true, nullable = false)
    String nameEn;

    @ManyToOne
    Region region;
}
