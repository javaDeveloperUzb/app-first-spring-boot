package uz.pdp.appfirstspringbootl39.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@PackagePrivate
@Entity
public class Region {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Column(unique = true, nullable = false)
    String nameUz;

    @Column(unique = true, nullable = false)
    String nameRu;

    @Column(unique = true, nullable = false)
    String nameEn;

    @ManyToOne
    Country country;

    @OneToMany(mappedBy = "region")
    List<District> districts;

}
