package uz.pdp.appfirstspringbootl39.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@PackagePrivate
@Entity
@Table(name = "country")
public class Country {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Column(unique = true, nullable = false)
    String nameUz;

    @Column(unique = true, nullable = false)
    String nameRu;

    @Column(unique = true, nullable = false)
    String nameEn;

    @OneToMany(mappedBy = "country")
    List<Region> regionList;


}
