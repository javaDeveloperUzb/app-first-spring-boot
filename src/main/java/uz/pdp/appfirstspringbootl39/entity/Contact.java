package uz.pdp.appfirstspringbootl39.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.persistence.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@PackagePrivate
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    
    @Column(nullable = false)
    String fistName;

    String lastName;

    String middleName;

    @Column(nullable = false, unique = true, length = 13)
    String number;

}
