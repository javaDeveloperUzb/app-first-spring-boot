package uz.pdp.appfirstspringbootl39.payload;

import lombok.Data;
import lombok.experimental.PackagePrivate;

import javax.validation.constraints.NotBlank;

@Data
@PackagePrivate
public class ReqCountry {
    @NotBlank
    String nameUz;
    @NotBlank
    String nameRu;
    @NotBlank
    String nameEn;
}
