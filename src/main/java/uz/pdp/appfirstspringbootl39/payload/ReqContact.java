package uz.pdp.appfirstspringbootl39.payload;

import lombok.Data;
import lombok.experimental.PackagePrivate;

@Data
@PackagePrivate
public class ReqContact {
    String firstName;
    String lastName;
    String middleName;
    String number;
}
