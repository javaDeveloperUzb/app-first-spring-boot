package uz.pdp.appfirstspringbootl39.payload;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.experimental.PackagePrivate;

import javax.validation.constraints.NotBlank;

@Data
@PackagePrivate
public class ReqDistrict {
    @NotBlank
    String nameUz;
    @NotBlank
    String nameRu;
    @NotBlank
    String nameEn;
    @NotNull
    Integer regionId;
}
