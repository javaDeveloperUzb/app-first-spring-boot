package uz.pdp.appfirstspringbootl39;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppFirstSpringBootApplicationLesson39 {

    public static void main(String[] args) {
        SpringApplication.run(AppFirstSpringBootApplicationLesson39.class, args);
    }

}
