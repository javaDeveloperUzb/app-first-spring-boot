package uz.pdp.appfirstspringbootl39.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appfirstspringbootl39.entity.Contact;
import uz.pdp.appfirstspringbootl39.payload.ReqContact;
import uz.pdp.appfirstspringbootl39.repository.ContactRepository;
import uz.pdp.appfirstspringbootl39.service.ContactService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
public class ContactController {
    @Autowired
    ContactService contactService;
    @Autowired
    ContactRepository contactRepository;

    public ContactController(ContactService contactService) {
        this.contactService = contactService;
    }

//    @RequestMapping(value = "/contact", method = RequestMethod.POST)
//    public boolean addContact(HttpServletRequest httpServletRequest) {
//        String firstName = httpServletRequest.getParameter("firstName");
//        String lastName = httpServletRequest.getParameter("lastName");
//        String middleName = httpServletRequest.getParameter("middleName");
//        String number = httpServletRequest.getParameter("number");
//        return contactService.saveContact(firstName, lastName, middleName, number);
//    }

    @RequestMapping(value = "/contact", method = RequestMethod.POST)
    @ResponseBody
    public boolean addContact(@RequestBody ReqContact reqContact) {
        return contactService.saveContact(reqContact);
    }

    @RequestMapping(value = "/contact", method = RequestMethod.GET)
    public String getContactPage() {
        return "ketmon";
    }

    @RequestMapping(value = "/contact/list", method = RequestMethod.GET)
    @ResponseBody
    public List<Contact> contacts() {
        List<Contact> contacts = contactService.getContacts();
        return contacts;
    }

    @RequestMapping(value = "/contact/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Contact getContact(@PathVariable Integer id) {
        Optional<Contact> optionalContact = contactRepository.findById(id);
        return optionalContact.orElseGet(Contact::new);
    }

    @RequestMapping(value = "/contact/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public boolean editContact(@PathVariable Integer id,@Valid @RequestBody ReqContact reqContact) {
        boolean editContact = contactService.editContact(id, reqContact);
        return editContact;
    }

    @RequestMapping(value = "/contact/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public boolean deleteContact(@PathVariable Integer id) {
        contactRepository.deleteById(id);
        return true;
    }
}
