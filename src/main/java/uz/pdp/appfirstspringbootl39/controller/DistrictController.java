package uz.pdp.appfirstspringbootl39.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appfirstspringbootl39.payload.ReqDistrict;
import uz.pdp.appfirstspringbootl39.repository.DistrictRepository;
import uz.pdp.appfirstspringbootl39.service.DistrictService;

import javax.validation.Valid;

@Controller
@RequestMapping("/district")
public class DistrictController {
    @Autowired
    DistrictService districtService;

    @Autowired
    DistrictRepository districtRepository;

    @PostMapping
    public HttpEntity<?> addDistrict(@Valid @RequestBody ReqDistrict reqDistrict) {
        boolean addDistrict = districtService.addDistrict(reqDistrict);
        return ResponseEntity.ok().body(addDistrict);
    }

    @GetMapping
    public HttpEntity<?> getDistrict() {
        return ResponseEntity.ok().body(districtService.getDistricts());
    }

    @GetMapping("/all")
    public HttpEntity<?> getDistrictAll() {
        return ResponseEntity.ok().body(districtRepository.findAll());
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getOneDistrict(@PathVariable Integer id) {
        return ResponseEntity.ok().body(districtRepository.findById(id));
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteDistrict(@PathVariable Integer id) {
        districtRepository.deleteById(id);
        return ResponseEntity.ok().body(id + " - id lik district o'chirildi");
    }
}
