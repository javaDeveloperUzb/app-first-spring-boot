package uz.pdp.appfirstspringbootl39.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appfirstspringbootl39.payload.ReqRegion;
import uz.pdp.appfirstspringbootl39.repository.RegionRepository;
import uz.pdp.appfirstspringbootl39.service.RegionService;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/region")
public class RegionController {
    @Autowired
    RegionService regionService;
    @Autowired
    RegionRepository regionRepository;

    @PostMapping
    public HttpEntity<?> addRegion(@Valid @RequestBody ReqRegion reqRegion) {
        regionService.addRegion(reqRegion);
        return ResponseEntity.ok("Saqlandi");
    }

    @GetMapping
    public HttpEntity<?> getRegion() {
        return ResponseEntity.ok().body(regionService.getRegions());
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getOneRegion(@PathVariable Integer id) {
        return ResponseEntity.ok().body(regionRepository.findById(id));
    }

    @PutMapping("/{id}")
    public boolean editRegion(@PathVariable Integer id, @Valid @RequestBody ReqRegion reqRegion) {
        boolean editRegion = regionService.editRegion(id, reqRegion);
//        return ResponseEntity.ok("O'zgartirildi");
        return editRegion;
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteRegion(@PathVariable Integer id) {
        regionRepository.deleteById(id);
        return ResponseEntity.ok(id + " - id lik Region O'chirildi");
    }
}
