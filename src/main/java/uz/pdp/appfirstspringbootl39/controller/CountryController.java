package uz.pdp.appfirstspringbootl39.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appfirstspringbootl39.payload.ReqCountry;
import uz.pdp.appfirstspringbootl39.repository.CountryRepository;
import uz.pdp.appfirstspringbootl39.service.CountryService;

import javax.validation.Valid;
import javax.ws.rs.Path;

@Controller
@RequestMapping("/country")
public class CountryController {
    @Autowired
    CountryService countryService;
    @Autowired
    CountryRepository countryRepository;

    @PostMapping
    public HttpEntity<?> addCountry(@Valid @RequestBody ReqCountry reqCountry) {
        countryService.addCountry(reqCountry);
        return ResponseEntity.ok().body(reqCountry.getNameUz() + " davlat Saqlandi");
    }

    @GetMapping
    public HttpEntity<?> getCountries() {
        return ResponseEntity.ok().body(countryRepository.findAll());
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getOneCountry(@PathVariable Integer id) {
        return ResponseEntity.ok().body(countryRepository.findById(id));
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editCountry(@PathVariable Integer id, @RequestBody ReqCountry reqCountry) {
        countryService.editContact(id, reqCountry);
        return ResponseEntity.ok().body("Ozgartirildi");
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteCountry(@PathVariable Integer id) {
        countryRepository.deleteById(id);
        return ResponseEntity.ok().body("ochirildi");
    }
}
